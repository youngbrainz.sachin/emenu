////
////  CoreDataManager.swift
////  PersistentTodoList
////
////  Created by Alok Upadhyay on 30/03/2018.
////  Copyright © 2017 Alok Upadhyay. All rights reserved.
////
// 
//import Foundation
//import CoreData
//import UIKit
// 
//class CoreDataManager {
//
//    //1
//    static let sharedManager = CoreDataManager()
//    private init() {}
//
//    //2
//    lazy var persistentContainer: NSPersistentContainer = {
//        let container = NSPersistentContainer(name: "FleetRoot")
//        container.loadPersistentStores(completionHandler: { (storeDescription,error) in
//            if let error = error as NSError? {
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    //3
//    func saveContext () {
//        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }
//
//
//    /*Insert*/
//    func insertNotification(notification_id: String, title: String, date: String,message: String,studentID: String,transportationType: String,broadCastType: String,timeStamp: String)->BroadCadstMessages? {
//        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
//        let entity = NSEntityDescription.entity(forEntityName: "BroadCadstMessages",in: managedContext)!
//        let timeZoneList = NSManagedObject(entity: entity,insertInto: managedContext)
//
//        timeZoneList.setValue(notification_id, forKeyPath: "notification_id")
//        timeZoneList.setValue(title, forKeyPath: "title")
//        timeZoneList.setValue(date, forKeyPath: "date")
//        timeZoneList.setValue(message, forKeyPath: "message")
//        timeZoneList.setValue(studentID, forKeyPath: "studentID")
//        timeZoneList.setValue(transportationType, forKeyPath: "transportationType")
//        timeZoneList.setValue(broadCastType, forKeyPath: "broadCastType")
//        timeZoneList.setValue(timeStamp, forKeyPath: "timeStamp")
//
//        do {
//            try managedContext.save()
//            return timeZoneList as? BroadCadstMessages
//        } catch let error as NSError {
//            print("Could not save. \(error), \(error.userInfo)")
//            return nil
//        }
//    }
//
//    /*delete*/
//    func delete(time_zone_db : BroadCadstMessages){
//        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
//        do {
//            managedContext.delete(time_zone_db)
//        } catch {
//            print(error)
//        }
//
//        do {
//            try managedContext.save()
//        } catch {
//        }
//    }
//
//    func fetchSingleMessage(notification_id: String) -> Bool {
//        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName:"BroadCadstMessages")
//        
//        fetchRequest.predicate = NSPredicate(format: "notification_id == %@" ,notification_id)
//
//        do {
//            let time_zone_db = try managedContext.fetch(fetchRequest)
//            if time_zone_db.count == 0{
//                return false
//            }
//            return true
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//            return false
//        }
//    }
//    
//    func fetchMessages(studentID: String, date: String) -> [BroadCadstMessages]?{
//        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName:"BroadCadstMessages")
////        fetchRequest.predicate = NSPredicate(format: "studentID == %@ AND date == %@" ,studentID, date)
////        fetchRequest.predicate = NSPredicate(format: "date == %@" , date)
//
//        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
//        do {
//            let time_zone_db = try managedContext.fetch(fetchRequest)
//            return time_zone_db as? [BroadCadstMessages]
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//            return nil
//        }
//    }
//
//    func delete(date: String) -> [BroadCadstMessages]? {
//        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
//
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName:"BroadCadstMessages")
//        fetchRequest.predicate = NSPredicate(format: "date == %@" ,date)
//        do {
//            let item = try managedContext.fetch(fetchRequest)
//            var arrRemovedPeople = [BroadCadstMessages]()
//            for i in item {
//                managedContext.delete(i)
//                try managedContext.save()
//                arrRemovedPeople.append(i as! BroadCadstMessages)
//            }
//            return arrRemovedPeople
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//            return nil
//        }
//    }
//}
