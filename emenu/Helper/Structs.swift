//
//  Enum.swift
//  FleetRoot
//
//  Created by Dharam YB on 17/01/20.
//  Copyright © 2020 YB. All rights reserved.
//

import Foundation

struct genderStatus{
    static let NotSpecified = 0
    static let Male = 1
    static let Female = 2
}

struct dateFormatter {
    static let yyyy_MM_dd_HH_mm_ss_ZZZ = "yyyy-MM-dd HH:mm:ss ZZZ" //2009-10-09 08:39:20 +0000
    static let yyyy = "yyyy" //2009
    static let EEE_MMMM_dd = "EEE, MMMM dd" // Fri, January 17
    static let MMM_dd_yyyy = "MMM dd,yyyy" // Jan 17,2020
    static let MMM_dd_yyyy_hh_mm_a = "MMM dd,yyyy - hh:mm a" // Jan 17,2020 - 10:00AM
    static let yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss" //2020-01-21 00:00:00
    static let yyyy_MM_dd_HH_MM_ss = "yyyy-MM-dd HH:MM:ss" //2020-01-21 00:00:00
    static let yyyy_MM_dd = "yyyy-MM-dd" //2020-01-21
    static let yyyy_MM_ddWithSlash = "yyyy/MM/dd" //2020/01/21
    static let yyyy_MM_dd_T_HH_mm_ssXXX = "yyyy-MM-dd'T'HH:mm:ssXXX"//1999-03-22T05:06:07+01:00
    static let yyyy_MM_dd_T_HH_mm_ss_SSSXXX = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX" //1999-03-22T05:06:07.000+01:00
    static let yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss"
    static let MM_dd_yyyy_hh_mm_ss_a = "MM/dd/yyyy hh:mm:ss a" //01/21/2020 09:20:22 AM
    static let MM_dd_yyyy_With_Slash = "MM/dd/yyyy" //01/21/2020
    static let MM = "MM" //01
    static let dd_MMM_yyyy = "dd MMM yyyy" //31 jan 2020
    static let dd_MMM_yyyy_hh_mm_a = "dd MMM yyyy hh:mm a" //31 jan 2020 06:40 PM
}

struct defaulID{
    static let defaultIdForall = "00000000-0000-0000-0000-000000000000"
}

struct newIDCardStatus{
    static let Pending = 0
    static let Approved = 1
    static let Rejected = 2
}

struct incidentReportStatus{
    static let Pending = 0
    static let Resolved = 1
}
