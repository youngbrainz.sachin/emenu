//
//  Common.swift
//  SMA
//
//  Created by MacBook Air 002 on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit
import LocalAuthentication

struct Common {

    static func setAppLocale(index:Int){
        if index == 0{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        //self.view.window!.windowScene!
        if #available(iOS 13.0, *) {
            appDel.window = UIWindow(windowScene: (findtopViewController()?.view.window?.windowScene)!)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "SplashVC") as! SplashVC
            appDel.navController = UINavigationController(rootViewController: viewController)
            appDel.navController?.isNavigationBarHidden = true
            appDel.window?.rootViewController = appDel.navController
            appDel.window!.overrideUserInterfaceStyle = .light
            appDel.window?.makeKeyAndVisible()
        }else{
            appDel.displayScreen()
        }
    }
    
    static func shadowWithCorner(_ control : UIControl,cornerRadius:CGFloat)
    {
        control.layer.masksToBounds = true
        control.layer.cornerRadius = cornerRadius
        control.clipsToBounds = false
        control.layer.shadowColor = UIColor.gray.cgColor
        control.layer.shadowOpacity = 0.5
        control.layer.shadowOffset = CGSize.zero
        control.layer.shadowRadius = 5
    }
    
    static func shadowForVeiw(_ control : UIView, radius:CGFloat,opacity:Float,shadowRadius:CGFloat)
    {
        control.layer.masksToBounds = true
        control.layer.cornerRadius = radius
        control.clipsToBounds = false
        control.layer.shadowColor = UIColor.gray.cgColor
        control.layer.shadowOpacity = opacity
        control.layer.shadowOffset = CGSize.zero
        control.layer.shadowRadius = shadowRadius
    }
    
    static func textfieldShadow(_ control : UITextField)
    {
        control.layer.backgroundColor = UIColor.white.cgColor
        control.layer.masksToBounds = false
        control.layer.cornerRadius = 4.0
        control.layer.shadowRadius = 3.0
        control.layer.shadowColor = UIColor.lightGray.cgColor
        control.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        control.layer.shadowOpacity = 0.4
    }
    
    static func getCountryList() -> [[String:Any]]
    {
        do
        {
            if let file = Bundle.main.url(forResource: "countries", withExtension: "json")
            {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any]
                {
                    let data = object["countries"] as? [[String:Any]]
                    return data!
                }
                else if let object = json as? [Any]
                {
                    print(object)
                }
                else
                {
                    print("JSON is invalid")
                }
            }
            else
            {
                print("no file")
            }
        }
        catch
        {
            print(error.localizedDescription)
        }
        return []
    }
    
    private static let suffix = ["", "K", "M", "B", "T", "P", "E"]
    
    public static func formatNumber(_ number: Double) -> String{
        var index = 0
        var value = number
        while((value / 1000) >= 1){
            value = value / 1000
            index += 1
        }
        return String(format: "%.1f%@", value, suffix[index])
    }
}

func setContiner(VC aVC: String, nibClass: UIViewController.Type, parent: UIViewController.Type, container: UIView, newController : ((UIViewController)->())? = nil) {
     let aVC = nibClass.init(nibName: aVC, bundle: .main)
    
    newController?(aVC)
    
    for aView in container.subviews {
        aView.removeFromSuperview()
    }
    
    aVC.view.frame = container.bounds
    container.addSubview(aVC.view)
}

 func setContinerOther(VC aVC: String, storyboardName:String, parent : UIViewController, container : UIView, newController : ((UIViewController)->())? = nil) {
    let st = UIStoryboard.init(name: storyboardName, bundle: nil)
    let aVC = st.instantiateViewController(withIdentifier: aVC)
    
    newController?(aVC)
    
    for aView in container.subviews{
        aView.removeFromSuperview()
    }
    
    for aChildVC in parent.children{
        aChildVC.removeFromParent()
    }
    
    aVC.view.frame = container.bounds
    container.addSubview(aVC.view)
    parent.addChild(aVC)
}

func findtopViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
        return findtopViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
        if let selected = tabController.selectedViewController {
            return findtopViewController(controller: selected)
        }
    }
    if let presented = controller?.presentedViewController {
        return findtopViewController(controller: presented)
    }
    return controller
}

func resetDefaults() {
    let defaults = UserDefaults.standard
    let dictionary = defaults.dictionaryRepresentation()
    dictionary.keys.forEach { key in
        defaults.removeObject(forKey: key)
    }
}

func alertController(message: String , controller: UIViewController)
{
    let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
    
    let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        controller.dismiss(animated: true, completion: nil)
    }
    
    alertController.addAction(action1)
    controller.present(alertController, animated: true, completion: nil)
}

func getCountryCodeFromDialcode(dialCode: String) -> [String: Any] {
    let countryitem = Common.getCountryList()
    var country = [String: Any]()
    for item in countryitem{
        if item["code"] as? String == dialCode{
            country = item
        }
    }
    return country
}

extension Dictionary {

    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }

    func printJson() {
        print(json)
    }

}

func toJsonString(json: [String:String]) -> String{
    let invalidJson = "Not a valid JSON"
    var error : NSError?

    let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])

    return String(data: jsonData, encoding: String.Encoding.utf8) ?? invalidJson

//    do {
//        let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
//        return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
//    } catch {
//        return invalidJson
//    }
}

func toJsonObjectFromString(jsonString: String) -> [String:Any]{
    if let data = jsonString.data(using: .utf8) {
        do {
            return try (JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:])
        } catch {
            print(error.localizedDescription)
        }
    }
    return [:]
}

func getCurrencySymbol(forCurrencyCode code: String) -> String? {
    let locale = NSLocale(localeIdentifier: code)
    if locale.displayName(forKey: .currencySymbol, value: code) == code {
        let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
        return newlocale.displayName(forKey: .currencySymbol, value: code)
    }
    return locale.displayName(forKey: .currencySymbol, value: code)
}

func isValidEmail(testStr:String) -> Bool
{
    let emailTest = NSPredicate(format:"SELF MATCHES %@", Email_RegEx)
    return emailTest.evaluate(with: testStr)
}


func addBlurBackground(control: UIImageView)  {
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = control.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    control.addSubview(blurEffectView)
}

func setButtonRound(control:UIButton){
    control.layer.cornerRadius = control.frame.size.width / 2
    control.clipsToBounds = true
}

func setViewCorner(control:UIView){
    control.layer.cornerRadius = control.frame.size.height / 2
    control.clipsToBounds = true
}

func addCorner(control: UIView, corner: UIRectCorner, size: CGSize)  {
    let rectShape1 = CAShapeLayer()
    rectShape1.bounds = control.frame
    rectShape1.position = control.center
    let frame = CGRect (x: 0, y: control.bounds.origin.y, width: control.bounds.size.width, height: control.bounds.size.height)
    rectShape1.path = UIBezierPath(roundedRect: frame, byRoundingCorners: corner, cornerRadii: size).cgPath
    control.layer.mask = rectShape1
}

func addCornerOnBottomLeftTopRight(control: UIView)  {
    let rectShape1 = CAShapeLayer()
    rectShape1.bounds = control.frame
    rectShape1.position = control.center
    let frame = CGRect (x: 0, y: control.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: control.bounds.size.height)
    rectShape1.path = UIBezierPath(roundedRect: frame, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 30, height: 30)).cgPath
    control.layer.mask = rectShape1
}

func localToUTC(date:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.timeZone = TimeZone.current

    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = "H:mm:ss"

    return dateFormatter.string(from: dt!)
}

func UTCToLocal(date:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "H:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "h:mm a"

    return dateFormatter.string(from: dt!)
}

func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
    
    let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date, to: currentDate)
    
    if let year = interval.year, year > 0 {
        return year == 1 ? "\(year)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "year_ago", comment: "") :
            "\(year)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "years_ago", comment: "")
    }else if let month = interval.month, month > 0 {
        return month == 1 ? "\(month)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "month_ago", comment: "") :
            "\(month)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "months_ago", comment: "")
    }else if let day = interval.day, day > 0 {
        return day == 1 ? "\(day)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "day_ago", comment: "") :
            "\(day)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "days_ago", comment: "")
    }else if var hour = interval.hour, hour > 0 {
        if let minute = interval.minute, minute > 0 {
            hour += 1
            return hour == 1 ? "\(hour)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "hr_ago", comment: "") :
                "\(hour)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "hrs_ago", comment: "")
        }
        return hour == 1 ? "\(hour)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "hr_ago", comment: "") :
            "\(hour)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "hrs_ago", comment: "")
    }else if let minute = interval.minute, minute > 0 {
        return minute == 1 ? "\(minute)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "min_ago", comment: "") :
            "\(minute)" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "mins_ago", comment: "")
    }else {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: "just_now", comment: "")
    }
}

//func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
//    let calendar = Calendar.current
//    let now = currentDate
//    let earliest = (now as NSDate).earlierDate(date)
//    let latest = (earliest == now) ? date : now
//    let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
//
//    if (components.year! >= 2) {
//        return "\(components.year!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "years_ago", comment: "")
//    } else if (components.year! >= 1){
//        if (numericDates){
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "year_ago", comment: "")
//        } else {
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "last_year", comment: "")
//        }
//    } else if (components.month! >= 2) {
//        return "\(components.month!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "months_ago", comment: "")
//    } else if (components.month! >= 1){
//        if (numericDates){
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "month_ago", comment: "")
//        } else {
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "last_month", comment: "")
//        }
//    } else if (components.weekOfYear! >= 2) {
//        return "\(components.weekOfYear!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "weeks_ago", comment: "")
//    } else if (components.weekOfYear! >= 1){
//        if (numericDates){
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "week_ago", comment: "")
//        } else {
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "last_week", comment: "")
//        }
//    } else if (components.day! >= 2) {
//        return "\(components.day!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "days_ago", comment: "")
//    } else if (components.day! >= 1){
//        if (numericDates){
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "day_ago", comment: "")
//        } else {
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "last_day", comment: "")
//        }
//    } else if (components.hour! >= 2) {
//        return "\(components.hour!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "hrs_ago", comment: "")
//    } else if (components.hour! >= 1){
//        if (numericDates){
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "hr_ago", comment: "")
//        } else {
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "an_hr_ago", comment: "")
//        }
//    } else if (components.minute! >= 2) {
//        return "\(components.minute!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "mins_ago", comment: "")
//    } else if (components.minute! >= 1){
//        if (numericDates){
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "min_ago", comment: "")
//        } else {
//            return LocalizationSystem.sharedInstance.localizedStringForKey(key: "a_min_ago", comment: "")
//        }
//    } else if (components.second! >= 3) {
//        return "\(components.second!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "sec_ago", comment: "")
//    } else {
//        return LocalizationSystem.sharedInstance.localizedStringForKey(key: "just_now", comment: "")
//    }
//}

func convertToTimeInterval(_ timeString:String) -> TimeInterval {
    guard !timeString.isEmpty else {
        return 0
    }

    var interval:Double = 0

    let parts = timeString.components(separatedBy: ":")
    for (index, part) in parts.reversed().enumerated() {
        interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
    }

    return interval
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

//MARK:- Touch ID
func authenticationWithTouchID(completion: (()->())!) {
    let localAuthenticationContext = LAContext()
    localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
    var authError: NSError?
    let reasonString = "Authenticate with Touch ID"
    if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reasonString) { success, evaluateError in
            if success {
                completion()
            } else {
                guard let error = evaluateError else {
                    return
                }
                print(evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
            }
        }
    } else {
        guard let error = authError else {
            return
        }
        print(evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
    }
}

func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
    var message = ""
    if #available(iOS 11.0, macOS 10.13, *) {
        switch errorCode {
        case LAError.biometryNotAvailable.rawValue:
            message = "Authentication could not start because the device does not support biometric authentication."
            
        case LAError.biometryLockout.rawValue:
            message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
            
        case LAError.biometryNotEnrolled.rawValue:
            message = "Authentication could not start because the user has not enrolled in biometric authentication."
            
        default:
            message = "Did not find error code on LAError object"
        }
    } else {
        switch errorCode {
        case LAError.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.touchIDNotEnrolled.rawValue:
            message = "TouchID is not enrolled on the device"
            
        default:
            message = "Did not find error code on LAError object"
        }
    }
    return message;
}
func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
    
    var message = ""
    
    switch errorCode {
        
    case LAError.authenticationFailed.rawValue:
        message = "The user failed to provide valid credentials"
        
    case LAError.appCancel.rawValue:
        message = "Authentication was cancelled by application"
        
    case LAError.invalidContext.rawValue:
        message = "The context is invalid"
        
    case LAError.notInteractive.rawValue:
        message = "Not interactive"
        
    case LAError.passcodeNotSet.rawValue:
        message = "Passcode is not set on the device"
        
    case LAError.systemCancel.rawValue:
        message = "Authentication was cancelled by the system"
//        authenticationWithTouchID(completion: nil)
    case LAError.userCancel.rawValue:
        message = "The user did cancel"
//        authenticationWithTouchID(completion: nil)
    case LAError.userFallback.rawValue:
        message = "The user chose to use the fallback"
    default:
        message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
    }
    return message
}
