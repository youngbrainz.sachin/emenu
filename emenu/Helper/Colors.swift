//
//  Colors.swift
//  SMA
//
//  Created by Jignesh Kugashiya on 27/06/19.
//  Copyright © 2019 Youngbrainz. All rights reserved.
//

import UIKit

struct AppColors {
    static let headerColor: UIColor = #colorLiteral(red: 0.8100044131, green: 0.1214786544, blue: 0.08822783083, alpha: 1)
    static let bottomViewBackgColor: UIColor = #colorLiteral(red: 0.6073860526, green: 0.7309864163, blue: 0.8114548326, alpha: 1)
    static let cellTableBackgColor: UIColor = #colorLiteral(red: 0.005325558595, green: 0.6911382079, blue: 0.2869892418, alpha: 1)
}
