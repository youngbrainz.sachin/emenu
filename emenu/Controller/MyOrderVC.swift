//
//  MyOrderVC.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RSSelectionMenu

class MyOrderVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewCart: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    
    @IBOutlet weak var lblTableNo: UILabel!
    @IBOutlet weak var viewTableNo: UIView!
    @IBOutlet weak var viewMenu: UIView!
    
    @IBOutlet weak var lblTotol: UILabel!
    @IBOutlet weak var tblOrder: UITableView!
    
    @IBOutlet weak var lblTotolPriceTitle: UILabel!
    @IBOutlet weak var lblQtyTitle: UILabel!
    @IBOutlet weak var lblItemNameTitle: UILabel!
    @IBOutlet weak var lblTotolTitle: UILabel!
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnSaveOrder: UIButton!
    
    var strTableNo = String()
    var qty = [1,1,1,1,1]    
    
    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
        
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLocalization()
    }
       
    override func viewDidAppear(_ animated: Bool) {
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            addCorner(control: self.viewBack, corner: [.bottomLeft , .topLeft], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewCart, corner: [.topRight , .bottomRight], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewTableNo, corner: [.bottomLeft , .topLeft], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewMenu, corner: [.topRight , .bottomRight], size: CGSize (width: 10, height: 10))
        }else{
            addCorner(control: self.viewBack, corner: [.bottomRight , .topRight], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewCart, corner: [.topLeft , .bottomLeft], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewTableNo, corner: [.bottomRight , .topRight], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewMenu, corner: [.topLeft , .bottomLeft], size: CGSize (width: 10, height: 10))
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnLangTapped(_ sender: UIButton) {
        let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnCartTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
    
    @IBAction func btnTableNoTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func btnSaveOrderTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "TableOrderVC") as! TableOrderVC
        objTab.modalPresentationStyle = .overCurrentContext
        self.present(objTab, animated: false)
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
}

extension MyOrderVC:UITableViewDelegate,UITableViewDataSource{
    
    func setupLocalization(){
        self.btnBack.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "back", comment: ""), for: .normal)
        self.btnCart.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "cart", comment: ""), for: .normal)
        
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_orders", comment: "");
        
        self.lblTotolPriceTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "");
        self.lblQtyTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "quantity", comment: "");
        self.lblItemNameTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_name", comment: "");
        self.lblTotolTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total", comment: "");
        self.btnSaveOrder.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save_order", comment: ""), for: .normal)
        self.btnMenu.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_capital", comment: ""), for: .normal)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            self.btnBack.setImage(#imageLiteral(resourceName: "send"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
            
            self.btnCart.imageEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 20)
            self.btnCart.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);            
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            self.btnBack.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
            
            self.btnCart.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
        }
        
        self.lblTableNo.text = self.strTableNo
        setButtonRound(control: self.btnLang)
        self.tblOrder.tableFooterView = UIView()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.qty.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblOrder.dequeueReusableCell(withIdentifier: "CellOrder") as! CellOrder
        setButtonRound(control: cell.btnPlus)
        setButtonRound(control: cell.btnMinus)
        
        cell.btnPlus.tag = indexPath.row
        cell.btnMinus.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        //cell.txtViewDesc.text = "abcdefghijklmnopqrstuvwxyz-abcdefghijklmnopqrstuvwxyz-abcdefghijklmnopqrstuvwxyz-abcdefghijklmnopqrstuvwxyz"
        cell.heightTxtView.constant = cell.txtViewDesc.contentSize.height
        cell.lblQty.text = "\(self.qty[indexPath.row])"
        
//        cell.txtViewDesc.translatesAutoresizingMaskIntoConstraints = true
//        cell.txtViewDesc.sizeToFit()
//        cell.txtViewDesc.layoutIfNeeded()
//        cell.txtViewDesc.isScrollEnabled = false
//        cell.txtViewDesc.isUserInteractionEnabled = false
//        cell.txtViewDesc.textColor = AppColors.headerColor
        
        cell.blockReloadTxtview = { str in 
            UIView.setAnimationsEnabled(false)
            self.tblOrder.beginUpdates()
            self.tblOrder.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
        return cell
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func btnPlusTapped(_ sender: UIButton) {
        print(sender.tag)
        var countQty = self.qty[sender.tag]
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tblOrder.cellForRow(at: indexPath) as! CellOrder
        countQty = (countQty + 1)
        self.qty[sender.tag] = countQty
        cell.lblQty.text = "\(countQty)"        
    }
    
    @IBAction func btnMinusTapped(_ sender: UIButton) {
        print(sender.tag)
        var countQty = self.qty[sender.tag]
        if countQty > 1{
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = self.tblOrder.cellForRow(at: indexPath) as! CellOrder
            countQty = (countQty - 1)
            self.qty[sender.tag] = countQty
            cell.lblQty.text = "\(countQty)"
        }
    }
    
    @IBAction func btnDeleteTapped(_ sender: UIButton) {
        self.qty.remove(at: sender.tag)
        self.tblOrder.reloadData()
    }
}
