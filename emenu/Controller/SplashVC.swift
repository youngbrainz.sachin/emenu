//
//  ViewController.swift
//  emenu
//
//  Created by Dharam YB on 05/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import RSSelectionMenu

class SplashVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewTagLine: UIView!
    @IBOutlet weak var lblTagLine: UILabel!
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnMenu: UIButton!

    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
        
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLocalization()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnLangTapped(_ sender: UIButton) {
//        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "SelectLangVC") as! SelectLangVC
//        objTab.modalPresentationStyle = .overCurrentContext
//        self.present(objTab, animated: false)
        // create menu with data source -> here [String]
        let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in            
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnInfoTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "TableNoVC") as! TableNoVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
    
    @IBAction func btnSettingTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
}

extension SplashVC{
    func setupLocalization(){
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            self.btnMenu.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        }
        
        setButtonRound(control: self.btnLang)
        setButtonRound(control: self.btnInfo)
        setButtonRound(control: self.btnSetting)
        setButtonRound(control: self.btnMenu)
        setViewCorner(control: self.viewTagLine)
    }
}
