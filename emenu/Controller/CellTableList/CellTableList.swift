//
//  CellTableList.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit

class CellTableList: UICollectionViewCell {
 
    //MARK:- Variables & Outlets
    @IBOutlet weak var viewBackg: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        
    }
}
