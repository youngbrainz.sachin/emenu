//
//  CellLang.swift
//  emenu
//
//  Created by Dharam YB on 10/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit

class CellLang: UITableViewCell {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblTitle:UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
