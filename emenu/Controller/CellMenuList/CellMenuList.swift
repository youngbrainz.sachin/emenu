//
//  CellMenuList.swift
//  emenu
//
//  Created by Dharam YB on 05/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit

class CellMenuList: UICollectionViewCell {
    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    
    //MARK:- Default Methods
    override func awakeFromNib() {
        
    }
}
