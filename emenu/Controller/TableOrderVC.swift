//
//  TableOrderVC.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RSSelectionMenu

class TableOrderVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var btnLang: UIButton!
    
    @IBOutlet weak var viewBlur: UIView!
    
    @IBOutlet weak var lblTotolPriceTitle: UILabel!
    @IBOutlet weak var lblTotolTitle: UILabel!
    @IBOutlet weak var lblQtyTitle: UILabel!
    @IBOutlet weak var lblItemNameTitle: UILabel!
    
    @IBOutlet weak var lblTotol: UILabel!
    @IBOutlet weak var btnClearTable: UIButton!
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var tblOrder: UITableView!
    
    @IBOutlet weak var bottomBtnNext: NSLayoutConstraint!
    
    var SelectedIndexOfTextView = IndexPath()
    var qty = [1,1,1,1,1]
    
    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.setupLocalization()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        addCorner(control: self.btnDropDown, corner: [.bottomRight , .bottomLeft], size: CGSize (width: 12, height: 12))
        addCorner(control: self.viewBlur, corner: [.topLeft , .topRight, .bottomLeft , .bottomRight], size: CGSize (width: 70, height: 70))
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnDropDownTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func btnLangTapped(_ sender: UIButton) {
        let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnClearTableTapped(_ sender: UIButton) {
     
    }
}

extension TableOrderVC:UITableViewDelegate,UITableViewDataSource{
    func setupLocalization(){
        self.lblTotolPriceTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "");
        self.lblTotolTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total", comment: "");
        self.lblQtyTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "quantity", comment: "");
        self.lblItemNameTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_name", comment: "");
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "table_numbers", comment: "");
        self.btnClearTable.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "clear_table", comment: ""), for: .normal)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        }
        
        setButtonRound(control: self.btnLang)
        self.btnClearTable.layer.cornerRadius = 36
        view.isOpaque = false
        view.backgroundColor = .clear
        self.tblOrder.tableFooterView = UIView()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return qty.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblOrder.dequeueReusableCell(withIdentifier: "CellOrder") as! CellOrder
        setButtonRound(control: cell.btnPlus)
        setButtonRound(control: cell.btnMinus)
        
        cell.btnPlus.tag = indexPath.row
        cell.btnMinus.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        cell.heightTxtView.constant = cell.txtViewDesc.contentSize.height
        
        cell.txtViewDesc.tag = indexPath.row
        self.SelectedIndexOfTextView = IndexPath(row: cell.txtViewDesc.tag, section: indexPath.section)
        
        cell.lblQty.text = "\(self.qty[indexPath.row])"
        
        cell.blockReloadTxtview = { str in
            UIView.setAnimationsEnabled(false)
            self.tblOrder.beginUpdates()
            self.tblOrder.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func btnPlusTapped(_ sender: UIButton) {
        print(sender.tag)
        var countQty = self.qty[sender.tag]
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tblOrder.cellForRow(at: indexPath) as! CellOrder
        countQty = (countQty + 1)
        self.qty[sender.tag] = countQty
        cell.lblQty.text = "\(countQty)"
    }
    
    @IBAction func btnMinusTapped(_ sender: UIButton) {
        print(sender.tag)
        var countQty = self.qty[sender.tag]
        if countQty > 1{
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = self.tblOrder.cellForRow(at: indexPath) as! CellOrder
            countQty = (countQty - 1)
            self.qty[sender.tag] = countQty
            cell.lblQty.text = "\(countQty)"
        }
    }
    
    @IBAction func btnDeleteTapped(_ sender: UIButton) {
        self.qty.remove(at: sender.tag)
        self.tblOrder.reloadData()
    }
    
    @IBAction func btnEditTapped(_ sender: UIButton) {
        print(sender.tag)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.2, animations: {
                if self.view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize.height
                    self.tblOrder.scrollToRow(at: self.SelectedIndexOfTextView, at: .top, animated: true)
                }
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            UIView.animate(withDuration: 0.2, animations: {
                if self.view.frame.origin.y != 0{
                    self.view.frame.origin.y = 0
                }
            })
        }
    }
}
