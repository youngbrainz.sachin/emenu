//
//  CellOrder.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit

class CellOrder: UITableViewCell {

    @IBOutlet weak var lblItemName: UILabel!    
    @IBOutlet weak var lblItemQty: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var heightTxtView: NSLayoutConstraint!
    
    var blockReloadTxtview:((String)->())?
    
    var txtLength = 0
    var maxLength = 26
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtViewDesc.delegate = self
        self.txtViewDesc.textContainerInset = UIEdgeInsets.zero
        self.txtViewDesc.textContainer.lineFragmentPadding = 0
        self.txtViewDesc.text = "Enter note"
        self.txtViewDesc.textColor = .gray
        self.txtViewDesc.tintColor = #colorLiteral(red: 0.9588896632, green: 0.4196166396, blue: 0.3852206469, alpha: 1)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.txtViewDesc.textAlignment = .right
        }else{
            self.txtViewDesc.textAlignment = .left
        }
        
        self.lblItemQty.text = "\(self.txtLength)/\(self.maxLength)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension CellOrder:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.txtViewDesc.text == "Enter note"{
            self.txtViewDesc.text = ""
            self.txtViewDesc.textColor = #colorLiteral(red: 0.9588896632, green: 0.4196166396, blue: 0.3852206469, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.txtViewDesc.text == "" || self.txtViewDesc.text.isEmpty{
            self.txtViewDesc.text = "Enter note"
            self.txtViewDesc.textColor = .gray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.heightTxtView.constant = self.txtViewDesc.contentSize.height
        let str = textView.text + text
        self.blockReloadTxtview!(str)
       
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if textView.text.count > 0 && text.count == 0{
            let count = textView.text.count - 1
            self.lblItemQty.text = "\(count + text.count)/\(self.maxLength)"
        }else{
            if textView.text.count + text.count == 27{
                
            }else{
                self.lblItemQty.text = "\(textView.text.count + text.count)/\(self.maxLength)"
            }
        }
        
        print("txtCount",textView.text.count + text.count)
        print("updtCount",updatedText.count)
        if updatedText.count == 27{
            return false
        }
        return updatedText.count <= self.maxLength
    }
}
