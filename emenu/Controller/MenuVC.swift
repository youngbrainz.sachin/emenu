//
//  MenuVC.swift
//  emenu
//
//  Created by Dharam YB on 05/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import RSSelectionMenu

class MenuVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewCart: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    
    @IBOutlet weak var collviewMenuList: UICollectionView!
    
    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
    let tempArr = [["item":#imageLiteral(resourceName: "food1"),"itemName":"Food-1"],
                   ["item":#imageLiteral(resourceName: "food4"),"itemName":"Food-2"],
                   ["item":#imageLiteral(resourceName: "food2"),"itemName":"Food-3"],
                   ["item":#imageLiteral(resourceName: "food3"),"itemName":"Food-4"],
                   ["item":#imageLiteral(resourceName: "food1"),"itemName":"Food-5"],
                   ["item":#imageLiteral(resourceName: "food4"),"itemName":"Food-6"],
                   ["item":#imageLiteral(resourceName: "food2"),"itemName":"Food-7"],
                   ["item":#imageLiteral(resourceName: "food3"),"itemName":"Food-8"],
                   ["item":#imageLiteral(resourceName: "food1"),"itemName":"Food-9"],
                   ["item":#imageLiteral(resourceName: "food4"),"itemName":"Food-10"],
                   ["item":#imageLiteral(resourceName: "food2"),"itemName":"Food-11"],
                   ["item":#imageLiteral(resourceName: "food3"),"itemName":"Food-12"]]
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLocalization()
    }
       
    override func viewDidAppear(_ animated: Bool) {
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            addCorner(control: self.viewBack, corner: [.bottomLeft , .topLeft], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewCart, corner: [.topRight , .bottomRight], size: CGSize (width: 10, height: 10))
        }else{
            addCorner(control: self.viewBack, corner: [.bottomRight , .topRight], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewCart, corner: [.topLeft , .bottomLeft], size: CGSize (width: 10, height: 10))
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnLangTapped(_ sender: UIButton) {
        let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnCartTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
}


extension MenuVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func setupLocalization(){
        self.btnBack.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "back", comment: ""), for: .normal)
        self.btnCart.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "cart", comment: ""), for: .normal)
        
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "menu_small", comment: "");
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            self.btnBack.setImage(#imageLiteral(resourceName: "send"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
            
            self.btnCart.imageEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 20)
            self.btnCart.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            self.btnBack.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
            
            self.btnCart.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)            
        }
        setButtonRound(control: self.btnLang)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tempArr.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collviewMenuList.dequeueReusableCell(withReuseIdentifier: "CellMenuList", for: indexPath) as! CellMenuList
//        cell.imgBackg.image = self.tempArr[indexPath.item]["item"] as? UIImage
//        cell.lblTitle.text = self.tempArr[indexPath.item]["itemName"] as? String
        cell.backgroundColor = .red
        cell.imgBackg.backgroundColor = .green
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "SubMenuVC") as! SubMenuVC
        self.navigationController?.pushViewController(objTab, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 4) - 14), height: CGFloat((collectionView.frame.size.height / 3) - 20))
    }
}
