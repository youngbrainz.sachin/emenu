//
//  SettingVC.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RSSelectionMenu

class SettingVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var viewBack: UIView!
    
    @IBOutlet weak var btnBack: UIButton!    
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var viewSync: UIView!
    @IBOutlet weak var btnFind: UIButton!
    @IBOutlet weak var btnChangePass: UIButton!
    
    @IBOutlet weak var lblSync: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLocalization()
    }
       
    override func viewDidAppear(_ animated: Bool) {
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            addCorner(control: self.viewBack, corner: [.bottomLeft , .topLeft], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewSync, corner: [.bottomLeft , .topLeft], size: CGSize (width: 8, height: 8))
        }else{
            addCorner(control: self.viewBack, corner: [.bottomRight , .topRight], size: CGSize (width: 10, height: 10))
            addCorner(control: self.viewSync, corner: [.bottomRight , .topRight], size: CGSize (width: 8, height: 8))
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnLangTapped(_ sender: UIButton) {
       let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnChangePassTapped(_ sender: UIButton) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassVC") as! ChangePassVC
        objTab.modalPresentationStyle = .overCurrentContext
        self.present(objTab, animated: false)
    }
    
    @IBAction func btnFindTapped(_ sender: UIButton) {
        
    }
}

extension SettingVC:UITextFieldDelegate{
    func setupLocalization(){
        self.btnBack.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "back", comment: ""), for: .normal)
        
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "settings", comment: "");
        self.txtName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "user_name", comment: "");
        self.txtPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "");
        self.btnChangePass.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "change_password", comment: ""), for: .normal)
        self.lblInfo.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "setting_info", comment: "");
        self.lblSync.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sync", comment: "");
        self.lblAbout.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "about", comment: "");
        self.btnFind.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "find_us_on", comment: ""), for: .normal)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            self.lblInfo.textAlignment = .right
            
            self.txtName.textAlignment = .right
            self.txtPassword.textAlignment = .right
            self.btnBack.setImage(#imageLiteral(resourceName: "send"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            self.lblInfo.textAlignment = .left
            
            self.txtName.textAlignment = .left
            self.txtPassword.textAlignment = .left
            self.btnBack.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        }
        setButtonRound(control: self.btnLang)
        self.btnFind.layer.cornerRadius = 8
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
