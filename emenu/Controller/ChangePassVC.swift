//
//  ChangePassVC.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RSSelectionMenu

class ChangePassVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPoweredBy: UILabel!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var viewLang: UIView!
    
    @IBOutlet weak var txtPassword: UITextField!
    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonRound(control: self.btnLang)
        view.isOpaque = false
        view.backgroundColor = .clear
        self.setupLocalization()
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnLangTapped(_ sender: UIButton) {
        let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}

extension ChangePassVC:UITextFieldDelegate{
    func setupLocalization(){
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "settings", comment: "")
        self.txtPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
        self.lblPoweredBy.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "powered_by", comment: "")
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            self.txtPassword.textAlignment = .right
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            self.txtPassword.textAlignment = .left
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
