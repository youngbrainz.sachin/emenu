//
//  SelectLangVC.swift
//  emenu
//
//  Created by Dharam YB on 10/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit

class SelectLangVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var tblLang:UITableView!
    
    @IBOutlet weak var heightTbl:NSLayoutConstraint!
    
    var arrLang = ["English", "عربى"]
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.isOpaque = false
        view.backgroundColor = .clear
        self.tblLang.tableFooterView = UIView()
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Select_Language", comment: "");
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.heightTbl.constant = self.tblLang.contentSize.height + 50
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnCloseTapped(_ sender:UIButton){
        self.dismiss(animated: false)
    }
}

extension SelectLangVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLang.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblLang.dequeueReusableCell(withIdentifier: "CellLang") as! CellLang
        cell.lblTitle.text = self.arrLang[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        self.dismiss(animated: false)
        if indexPath.row == 0{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        if #available(iOS 13.0, *) {
            appDel.window = UIWindow(windowScene: self.view.window!.windowScene!)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "SplashVC") as! SplashVC
            appDel.navController = UINavigationController(rootViewController: viewController)
            appDel.navController?.isNavigationBarHidden = true
            appDel.window?.rootViewController = appDel.navController
            appDel.window!.overrideUserInterfaceStyle = .light
            appDel.window?.makeKeyAndVisible()
        }else{
            appDel.displayScreen()
        }
    }
}
