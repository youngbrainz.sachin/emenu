//
//  TableNoVC.swift
//  emenu
//
//  Created by Dharam YB on 06/02/20.
//  Copyright © 2020 YB. All rights reserved.
//

import UIKit
import RSSelectionMenu

class TableNoVC: UIViewController {

    //MARK:- Variables & Outlets
    @IBOutlet weak var imgBackg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var viewBack: UIView!
    
    @IBOutlet weak var btnBack: UIButton!    
    
    @IBOutlet weak var collviewTableList: UICollectionView!
    
    let data: [String] = ["English", "عربى"]
    var selectedNames: [String] = []
    let tempArr = ["Table-1",
                   "Table-2",
                   "Table-3",
                   "Table-4",
                   "Table-5",
                   "Table-6",
                   "Table-7",
                   "Table-8",
                   "Table-9",
                   "Table-10",
                   "Table-11",
                   "Table-12"]
    
    //MARK:- Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLocalization()
    }
       
    override func viewDidAppear(_ animated: Bool) {
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            addCorner(control: self.viewBack, corner: [.bottomLeft , .topLeft], size: CGSize (width: 10, height: 10))
        }else{
            addCorner(control: self.viewBack, corner: [.bottomRight , .topRight], size: CGSize (width: 10, height: 10))
        }
    }
    
    //MARK:- Button Tapped Events
    @IBAction func btnLangTapped(_ sender: UIButton) {
        let menu = RSSelectionMenu(dataSource: data) { (cell, name, indexPath) in
            cell.textLabel?.text = name
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .white
            cell.backgroundColor = AppColors.headerColor
        }
        menu.setSelectedItems(items: selectedNames) { (name, index, selected, selectedItems) in
            Common.setAppLocale(index: index)
        }
        menu.tableView?.backgroundColor = AppColors.headerColor
        menu.tableView?.separatorColor = .white
        menu.show(style: .popover(sourceView: self.viewLang, size: CGSize(width: 140, height: 78)), from: self)
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension TableNoVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func setupLocalization(){
        self.lblTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "table_numbers", comment: "")
        self.btnBack.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "back", comment: ""), for: .normal)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            self.btnLang.setTitle("Ar", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 28, bottom: 24, right: 18)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 12)
            self.btnBack.setImage(#imageLiteral(resourceName: "send"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        }else{
            self.btnLang.setTitle("En", for: .normal)
            self.btnLang.imageEdgeInsets = UIEdgeInsets(top: 24, left: 5, bottom: 24, right: 40)
            self.btnLang.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            self.btnBack.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            self.btnBack.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        }
        setButtonRound(control: self.btnLang)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tempArr.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collviewTableList.dequeueReusableCell(withReuseIdentifier: "CellTableList", for: indexPath) as! CellTableList
            cell.viewBackg.layer.cornerRadius = 30
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5{
            cell.viewBackg.backgroundColor = AppColors.headerColor
        }else{
            cell.viewBackg.backgroundColor = AppColors.cellTableBackgColor
        }
        cell.lblTitle.text = self.tempArr[indexPath.item]
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objTab = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
        objTab.strTableNo = self.tempArr[indexPath.item]
        self.navigationController?.pushViewController(objTab, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 50
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 30), height:CGFloat((collectionView.frame.size.height / 3) - 50))
        //
    }
}

